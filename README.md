# Atelier HTML

## Exercice 1:  

Créer un fichier index.html dont le résultat est comme le suivant:

![alt text](/img/maquette-index.png) 

la page index contient à la fin un lien vers une autre page contact.

Créer la page contact.html dont le résultat est comme le suivant:

![alt text](/img/maquette-contact.jpg) 

## Exercice 2:  

Créer un fichier menu.html dont le résultat est le suivant:

![alt text](/img/menu.jpg) 

la page menu contient 3 liens vers 3 pages que vous devez créer:

* connexion.html  
![alt text](/img/connexion.jpg) 

* identite.html  
![alt text](/img/identite.jpg) 

* details.html  
![alt text](/img/details.jpg) 


